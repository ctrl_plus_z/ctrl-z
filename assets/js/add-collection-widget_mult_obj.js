var $ = require('jquery');

// add-collection-widget_mult_obj.js
jQuery(document).ready(function () {
    jQuery('.add-another-collection-widget_mult_obj').click(function (e) {
        e.preventDefault();
        var list = jQuery(jQuery(this).attr('data-list'));
        // Try to find the counter of the list
        var counter = list.data('widget-counter') | list.children().length;
        // If the counter does not exist, use the length of the list
        if (!counter) { counter = list.children().length; }

        var numWidgets = list.attr('numWidgets');
        newWidgetStr = '';
        for(i=1; i<=numWidgets;  ++i) {
            // grab the prototype template
            prototype=('data-prototype_'+i);
            var newWidget = list.attr(prototype);
            // replace the "__name__" used in the id and name of the prototype
            // with a number that's unique to your emails
            // end name attribute looks like name="contact[emails][2]"
            newWidgetStr += newWidget.replace(/__name__/g, counter);
            // create a new list element and add it to the list
        }
        counter++;
        // Increase the counter
        // And store it, the length cannot be used if deleting widgets is allowed
        list.data('widget-counter', counter);
        var newElem = jQuery(list.attr('data-widget-tags')).html(newWidgetStr);
        newElem.appendTo(list);
    });
});
