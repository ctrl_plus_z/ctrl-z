<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ClickableType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\User;

class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     */
    public function default(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('Register', SubmitType::class)
            ->add('Login', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if($form->get('Register')->isClicked())
            {
                return $this->redirectToRoute('user_registration');
            }
            if($form->get('Login')->isClicked())
            {
                return $this->redirectToRoute('app_login');
            }
        }

        return $this->render('default.html.twig',
            array('form' => $form->createView())  
        );
    }
}
