<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Device;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ClickableType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class ConnectUser extends AbstractController
{
    /**
     * @Route("/connectUser", name="connectUser")
     */
    public function welcome(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('Email', EmailType::class)
            ->add('Device_Id', IntegerType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $device = $this->getDoctrine()->getRepository(Device::class)->find($data['Device_Id']);
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $data['Email']]);
            $user->addConnectedDevice($device);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($device);
            $entityManager->persist($user);
            $entityManager->flush();
            //            return $this->redirectToRoute('app_login');
        }
        return $this->render('connectUser.html.twig',
            array('form' => $form->createView())  
        );
    }

}
