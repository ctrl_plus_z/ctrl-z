<?php

// src/Controller/DeviceController.php
namespace App\Controller;

use App\Form\DeviceType;
use App\Entity\Device;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DeviceController extends AbstractController
{
    /**
     * @Route("/device", name="manage_device")
     */
    public function register(Request $request)
    {
        // 1) build the form
        $device = new Device();
        $form = $this->createForm(DeviceType::class, $device);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if($form->isValid()) {
                $entityManager = $this->getDoctrine()->getManager();
                $data = $form->getData();
                foreach ($data->getIot()->toArray() as $iot)
                {
                    //add initial value to the iot table
                    $table = $iot->createTable();
                    $entityManager->persist($table);
                }
                $entityManager->persist($device);
                $entityManager->flush();
                // ... do any other work - like sending them an email, etc
                // maybe set a "flash" success message for the user
                //$this->addFlash('success', 'Registration success! Please see your Email');
                return $this->redirectToRoute('homepage');
            }
            else
            {
                $data = $form->getData();
                print_r($data->getIot());
                $this->addFlash('error', 'Error occured in device registration');
            }
        }

        return $this->render(
            'device.html.twig',
            array('form' => $form->createView())
        );
    }
}
