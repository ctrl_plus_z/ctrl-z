<?php

namespace App\Controller;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ClickableType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\User;
use App\Security\LoginFormAuthenticator;

class UserWindow extends AbstractController
{
    /**
     * @Route("/user_window/{_user}", name="user_window")
     */
    public function default(Request $request, $_user)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $_user]);
        $devices = $user->getConnectedDevices();
        $builder = $this->createFormBuilder();
        foreach( $devices as $device)
        {
            $builder->add($device->getName(), SubmitType::class);
        }
        $form = $builder->getForm();

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            foreach( $devices as $device)
            {
                if ($form->get($device->getName())->isClicked())
                {
                    echo 'Hi there';
                    //                    return $this->redirectToRoute('app_login');
                }
            }
        }
        return $this->render('userView.html.twig',
            array('form' => $form->createView(), 'devices' => $devices)
        );
    }
}
