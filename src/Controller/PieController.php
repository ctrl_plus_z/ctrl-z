<?php

namespace App\Controller;
use App\Service\jsonToCSV;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PieController extends AbstractController
{
    /**
     * @Route("/pie", name="pie")
     */
    public function chart()
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery('SELECT p FROM App\Entity\Fruit p ORDER BY p.value DESC')
            ->setMaxResults(5);

        $fruits = $query->execute();
        $json = json_encode($fruits);
        $jc = new jsonToCSV;
        $csv = $jc->convert($json);
        return $this->render('chart_column.html.twig', array(
            'title' => 'Anychart PHP template',
            'chartData' => $csv,
            'chartTitle' => 'Top 5 fruits',
            'fruits' => json_encode($fruits),
        ));
    }

}
