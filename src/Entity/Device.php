<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DeviceRepository")
 * @Vich\Uploadable
 */
class Device
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $type;

    /**
     * @ORM\Column(type="float")
     */
    private $initialPrice;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $manufacturer;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $model;

    /**
     * @ORM\Column(type="datetime")
     */
    private $purchaseDate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Iot", mappedBy="device", cascade={"persist"})
     */
    private $iot;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $color;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $weight;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $serialNumber;

    /**
     * @ORM\Column(type="float")
     */
    private $height;

    /**
     * @ORM\Column(type="float")
     */
    private $width;

    /**
     * @ORM\Column(type="float")
     */
    private $thickness;

    /**
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $productRemarks;

    /**
     * @ORM\Column(type="float")
     */
    private $lifeTime;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $metrics = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $metricDefinition = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $metricSeverity = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $metricMin = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $metricMax = [];

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $metricMessage = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="connectedDevices")
     */
    private $user;
    /**
     * NOTE: This is not a mapped field of entity metadata, just a simple property.
     *
     * @Vich\UploadableField(mapping="product_image", fileNameProperty="imageName", size="imageSize")
     *
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="string", length=255)
     *
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(type="integer")
     *
     * @var integer
     */
    private $imageSize;

    /**
     * @ORM\Column(type="datetime")
     *
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct()
    {
        $this->iot = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getInitialPrice(): ?float
    {
        return $this->initialPrice;
    }

    public function setInitialPrice(float $initialPrice): self
    {
        $this->initialPrice = $initialPrice;

        return $this;
    }

    public function getManufacturer(): ?string
    {
        return $this->manufacturer;
    }

    public function setManufacturer(string $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getPurchaseDate(): ?\DateTimeInterface
    {
        return $this->purchaseDate;
    }

    public function setPurchaseDate(\DateTimeInterface $purchaseDate): self
    {
        $this->purchaseDate = $purchaseDate;

        return $this;
    }

    /**
     * @return Collection|Iot[]
     */
    public function getIot(): Collection
    {
        return $this->iot;
    }

    public function addIot(Iot $iot): self
    {
        if (!$this->iot->contains($iot)) {
            $this->iot[] = $iot;
            $iot->setDevice($this);
        }

        return $this;
    }

    public function removeIot(Iot $iot): self
    {
        if ($this->iot->contains($iot)) {
            $this->iot->removeElement($iot);
            // set the owning side to null (unless already changed)
            if ($iot->getDevice() === $this) {
                $iot->setDevice(null);
            }
        }

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getSerialNumber(): ?string
    {
        return $this->serialNumber;
    }

    public function setSerialNumber(string $serialNumber): self
    {
        $this->serialNumber = $serialNumber;

        return $this;
    }

    public function getHeight(): ?float
    {
        return $this->height;
    }

    public function setHeight(float $height): self
    {
        $this->height = $height;

        return $this;
    }

    public function getWidth(): ?float
    {
        return $this->width;
    }

    public function setWidth(float $width): self
    {
        $this->width = $width;

        return $this;
    }

    public function getThickness(): ?float
    {
        return $this->thickness;
    }

    public function setThickness(float $thickness): self
    {
        $this->thickness = $thickness;

        return $this;
    }

    public function getProductRemarks(): ?string
    {
        return $this->productRemarks;
    }

    public function setProductRemarks(?string $productRemarks): self
    {
        $this->productRemarks = $productRemarks;

        return $this;
    }

    public function getLifeTime(): ?float
    {
        return $this->lifeTime;
    }

    public function setLifeTime(float $lifeTime): self
    {
        $this->lifeTime = $lifeTime;

        return $this;
    }

    public function getMetrics(): ?array
    {
        return $this->metrics;
    }

    public function setMetrics(?array $metrics): self
    {
        $this->metrics = $metrics;

        return $this;
    }

    public function getMetricDefinition(): ?array
    {
        return $this->metricDefinition;
    }

    public function setMetricDefinition(?array $metricDefinition): self
    {
        $this->metricDefinition = $metricDefinition;

        return $this;
    }

    public function getMetricSeverity(): ?array
    {
        return $this->metricSeverity;
    }

    public function setMetricSeverity(?array $metricSeverity): self
    {
        $this->metricSeverity = $metricSeverity;

        return $this;
    }

    public function getMetricMin(): ?array
    {
        return $this->metricMin;
    }

    public function setMetricMin(?array $metricMin): self
    {
        $this->metricMin = $metricMin;

        return $this;
    }

    public function getMetricMax(): ?array
    {
        return $this->metricMax;
    }

    public function setMetricMax(?array $metricMax): self
    {
        $this->metricMax = $metricMax;

        return $this;
    }

    public function getMetricMessage(): ?array
    {
        return $this->metricMessage;
    }

    public function setMetricMessage(?array $metricMessage): self
    {
        $this->metricMessage = $metricMessage;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function setImageFile(?File $image = null): void
    {
        $this->imageFile = $image;
        if (null !== $image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
            $this->updatedAt = new \DateTimeImmutable();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setImageSize(?int $imageSize): void
    {
        $this->imageSize = $imageSize;
    }

    public function getImageSize(): ?int
    {
        return $this->imageSize;
    }
}
