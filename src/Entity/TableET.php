<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TableETRepository")
 */
class TableET
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timestamp;

    /**
     * @ORM\Column(type="float")
     */
    private $energy;

    /**
     * @ORM\Column(type="float")
     */
    private $temperature;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Iot", inversedBy="tableET")
     * @ORM\JoinColumn(nullable=false)
     */
    private $iot;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimestamp(): ?\DateTimeInterface
    {
        return $this->timestamp;
    }

    public function setTimestamp(\DateTimeInterface $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }

    public function getEnergy(): ?float
    {
        return $this->energy;
    }

    public function setEnergy(float $energy): self
    {
        $this->energy = $energy;

        return $this;
    }

    public function getTemperature(): ?float
    {
        return $this->temperature;
    }

    public function setTemperature(float $temperature): self
    {
        $this->temperature = $temperature;

        return $this;
    }

    public function getIot(): ?Iot
    {
        return $this->iot;
    }

    public function setIot(?Iot $iot): self
    {
        $this->iot = $iot;

        return $this;
    }

    public function initVal(?Iot $iot): self
    {
        $this->setTimestamp(new \DateTime("now"));
        $this->setTemperature(0);
        $this->setEnergy(0);
        $this->setIot($iot);
        return $this;
    }

}
