<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\IotRepository")
 */
class Iot
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Device", inversedBy="iot")
     */
    private $device;

    /**
     * @ORM\Column(type="string", length=512)
     */
    private $sensorType;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TableE", mappedBy="iot")
     */
    private $tableE;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TableT", mappedBy="iot")
     */
    private $tableT;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TableET", mappedBy="iot")
     */
    private $tableET;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\TableW", mappedBy="iot")
     */
    private $tableW;

    public function __construct()
    {
        $this->tableE = new ArrayCollection();
        $this->tableT = new ArrayCollection();
        $this->tableET = new ArrayCollection();
        $this->tableW = new ArrayCollection();
    }

   public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDevice(): ?Device
    {
        return $this->device;
    }

    public function setDevice(?Device $device): self
    {
        $this->device = $device;

        return $this;
    }

    public function getSensorType(): ?string
    {
        return $this->sensorType;
    }

    public function setSensorType(string $sensorType): self
    {
        $this->sensorType = $sensorType;

        return $this;
    }

    public function getTable(): Collection
    {
        if($this->getSensorType() == 'E')
        {
            return getTableE();
        }
        elseif($this->getSensorType() == 'T')
        {
            return getTableT();
        }
        elseif($this->getSensorType() == 'ET')
        {
            return getTableET();
        }
        elseif($this->getSensorType() == 'W')
        {
            return getTableW();
        }
        else
        {
            echo "Sensor Type not found";
            return getTableE();
        }
    }

    public function addTable($table): self
    {
        if($this->getSensorType() == 'E')
        {
            return addTableE($table);
        }
        elseif($this->getSensorType() == 'T')
        {
            return addTableT($table);
        }
        elseif($this->getSensorType() == 'ET')
        {
            return addTableET($table);
        }
        elseif($this->getSensorType() == 'W')
        {
            return addTableW($table);
        }
        else
        {
            echo "Sensor Type not found";
            return $this;
        }
    }

    public function removeTable($table): self
    {
        if($this->getSensorType() == 'E')
        {
            return removeTableE($table);
        }
        elseif($this->getSensorType() == 'T')
        {
            return removeTableT($table);
        }
        elseif($this->getSensorType() == 'ET')
        {
            return removeTableET($table);
        }
        elseif($this->getSensorType() == 'W')
        {
            return removeTableW($table);
        }
        else
        {
            echo "Sensor Type not found";
            return $this;
        }
    }

    public function createTable()
    {
        if($this->getSensorType() == 'E')
        {
            $table = new TableE();
            $table->initVal($this);
            return $table;
        }
        elseif($this->getSensorType() == 'T')
        {
            $table = new TableT();
            $table->initVal($this);
            return $table;
        }
        elseif($this->getSensorType() == 'ET')
        {
            $table = new TableET();
            $table->initVal($this);
            return $table;
        }
        elseif($this->getSensorType() == 'W')
        {
            $table = new TableW();
            $table->initVal($this);
            return $table;
        }
        else
        {
            echo "Sensor Type not found";
        }
    }


    /**
     * @return Collection|TableE[]
     */
    public function getTableE(): Collection
    {
        return $this->tableE;
    }

    public function addTableE(TableE $tableE): self
    {
        if (!$this->tableE->contains($tableE)) {
            $this->tableE[] = $tableE;
            $tableE->setIot($this);
        }

        return $this;
    }

    public function removeTableE(TableE $tableE): self
    {
        if ($this->tableE->contains($tableE)) {
            $this->tableE->removeElement($tableE);
            // set the owning side to null (unless already changed)
            if ($tableE->getIot() === $this) {
                $tableE->setIot(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TableT[]
     */
    public function getTableT(): Collection
    {
        return $this->tableT;
    }

    public function addTableT(TableT $tableT): self
    {
        if (!$this->tableT->contains($tableT)) {
            $this->tableT[] = $tableT;
            $tableT->setIot($this);
        }

        return $this;
    }

    public function removeTableT(TableT $tableT): self
    {
        if ($this->tableT->contains($tableT)) {
            $this->tableT->removeElement($tableT);
            // set the owning side to null (unless already changed)
            if ($tableT->getIot() === $this) {
                $tableT->setIot(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TableET[]
     */
    public function getTableET(): Collection
    {
        return $this->tableET;
    }

    public function addTableET(TableET $tableET): self
    {
        if (!$this->tableET->contains($tableET)) {
            $this->tableET[] = $tableET;
            $tableET->setIot($this);
        }

        return $this;
    }

    public function removeTableET(TableET $tableET): self
    {
        if ($this->tableET->contains($tableET)) {
            $this->tableET->removeElement($tableET);
            // set the owning side to null (unless already changed)
            if ($tableET->getIot() === $this) {
                $tableET->setIot(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|TableW[]
     */
    public function getTableW(): Collection
    {
        return $this->tableW;
    }

    public function addTableW(TableW $tableW): self
    {
        if (!$this->tableW->contains($tableW)) {
            $this->tableW[] = $tableW;
            $tableW->setIot($this);
        }

        return $this;
    }

    public function removeTableW(TableW $tableW): self
    {
        if ($this->tableW->contains($tableW)) {
            $this->tableW->removeElement($tableW);
            // set the owning side to null (unless already changed)
            if ($tableW->getIot() === $this) {
                $tableW->setIot(null);
            }
        }

        return $this;
    }
}
