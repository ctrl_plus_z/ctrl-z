<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181031193616 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE table_e ADD iot_id INT NOT NULL');
        $this->addSql('ALTER TABLE table_e ADD CONSTRAINT FK_9A0646A8F8063444 FOREIGN KEY (iot_id) REFERENCES iot (id)');
        $this->addSql('CREATE INDEX IDX_9A0646A8F8063444 ON table_e (iot_id)');
        $this->addSql('ALTER TABLE table_t ADD iot_id INT NOT NULL');
        $this->addSql('ALTER TABLE table_t ADD CONSTRAINT FK_F0B6665AF8063444 FOREIGN KEY (iot_id) REFERENCES iot (id)');
        $this->addSql('CREATE INDEX IDX_F0B6665AF8063444 ON table_t (iot_id)');
        $this->addSql('ALTER TABLE table_et ADD iot_id INT NOT NULL');
        $this->addSql('ALTER TABLE table_et ADD CONSTRAINT FK_5DFD7734F8063444 FOREIGN KEY (iot_id) REFERENCES iot (id)');
        $this->addSql('CREATE INDEX IDX_5DFD7734F8063444 ON table_et (iot_id)');
        $this->addSql('ALTER TABLE table_w ADD iot_id INT NOT NULL');
        $this->addSql('ALTER TABLE table_w ADD CONSTRAINT FK_69BF37E0F8063444 FOREIGN KEY (iot_id) REFERENCES iot (id)');
        $this->addSql('CREATE INDEX IDX_69BF37E0F8063444 ON table_w (iot_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE table_e DROP FOREIGN KEY FK_9A0646A8F8063444');
        $this->addSql('DROP INDEX IDX_9A0646A8F8063444 ON table_e');
        $this->addSql('ALTER TABLE table_e DROP iot_id');
        $this->addSql('ALTER TABLE table_et DROP FOREIGN KEY FK_5DFD7734F8063444');
        $this->addSql('DROP INDEX IDX_5DFD7734F8063444 ON table_et');
        $this->addSql('ALTER TABLE table_et DROP iot_id');
        $this->addSql('ALTER TABLE table_t DROP FOREIGN KEY FK_F0B6665AF8063444');
        $this->addSql('DROP INDEX IDX_F0B6665AF8063444 ON table_t');
        $this->addSql('ALTER TABLE table_t DROP iot_id');
        $this->addSql('ALTER TABLE table_w DROP FOREIGN KEY FK_69BF37E0F8063444');
        $this->addSql('DROP INDEX IDX_69BF37E0F8063444 ON table_w');
        $this->addSql('ALTER TABLE table_w DROP iot_id');
    }
}
