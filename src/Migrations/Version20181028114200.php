<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181028114200 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE device ADD iot LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', ADD param_reference_value LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', ADD param_name LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', DROP iot1, DROP iot2, DROP iot3, DROP iot4');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE device ADD iot1 INT NOT NULL, ADD iot2 INT DEFAULT NULL, ADD iot3 INT DEFAULT NULL, ADD iot4 INT DEFAULT NULL, DROP iot, DROP param_reference_value, DROP param_name');
    }
}
