<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181028210624 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE iot ADD device_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE iot ADD CONSTRAINT FK_D4AEBF094A4C7D4 FOREIGN KEY (device_id) REFERENCES device (id)');
        $this->addSql('CREATE INDEX IDX_D4AEBF094A4C7D4 ON iot (device_id)');
        $this->addSql('ALTER TABLE device DROP FOREIGN KEY FK_92FB68EF8063444');
        $this->addSql('DROP INDEX UNIQ_92FB68EF8063444 ON device');
        $this->addSql('ALTER TABLE device DROP iot_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE device ADD iot_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE device ADD CONSTRAINT FK_92FB68EF8063444 FOREIGN KEY (iot_id) REFERENCES iot (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_92FB68EF8063444 ON device (iot_id)');
        $this->addSql('ALTER TABLE iot DROP FOREIGN KEY FK_D4AEBF094A4C7D4');
        $this->addSql('DROP INDEX IDX_D4AEBF094A4C7D4 ON iot');
        $this->addSql('ALTER TABLE iot DROP device_id');
    }
}
