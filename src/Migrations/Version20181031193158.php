<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181031193158 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE iot DROP FOREIGN KEY FK_D4AEBF016CFB43E');
        $this->addSql('ALTER TABLE iot DROP FOREIGN KEY FK_D4AEBF047A1BD0');
        $this->addSql('ALTER TABLE iot DROP FOREIGN KEY FK_D4AEBF0E651E9B1');
        $this->addSql('ALTER TABLE iot DROP FOREIGN KEY FK_D4AEBF0FE6A84C4');
        $this->addSql('DROP INDEX UNIQ_D4AEBF0FE6A84C4 ON iot');
        $this->addSql('DROP INDEX UNIQ_D4AEBF016CFB43E ON iot');
        $this->addSql('DROP INDEX UNIQ_D4AEBF0E651E9B1 ON iot');
        $this->addSql('DROP INDEX UNIQ_D4AEBF047A1BD0 ON iot');
        $this->addSql('ALTER TABLE iot DROP table_e_id, DROP table_t_id, DROP table_et_id, DROP table_w_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE iot ADD table_e_id INT DEFAULT NULL, ADD table_t_id INT DEFAULT NULL, ADD table_et_id INT DEFAULT NULL, ADD table_w_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE iot ADD CONSTRAINT FK_D4AEBF016CFB43E FOREIGN KEY (table_t_id) REFERENCES table_t (id)');
        $this->addSql('ALTER TABLE iot ADD CONSTRAINT FK_D4AEBF047A1BD0 FOREIGN KEY (table_w_id) REFERENCES table_w (id)');
        $this->addSql('ALTER TABLE iot ADD CONSTRAINT FK_D4AEBF0E651E9B1 FOREIGN KEY (table_et_id) REFERENCES table_et (id)');
        $this->addSql('ALTER TABLE iot ADD CONSTRAINT FK_D4AEBF0FE6A84C4 FOREIGN KEY (table_e_id) REFERENCES table_e (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4AEBF0FE6A84C4 ON iot (table_e_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4AEBF016CFB43E ON iot (table_t_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4AEBF0E651E9B1 ON iot (table_et_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D4AEBF047A1BD0 ON iot (table_w_id)');
    }
}
