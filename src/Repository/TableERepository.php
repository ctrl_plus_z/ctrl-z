<?php

namespace App\Repository;

use App\Entity\TableE;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TableE|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableE|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableE[]    findAll()
 * @method TableE[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableERepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TableE::class);
    }

//    /**
//     * @return TableE[] Returns an array of TableE objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TableE
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
