<?php

namespace App\Repository;

use App\Entity\TableET;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TableET|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableET|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableET[]    findAll()
 * @method TableET[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableETRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TableET::class);
    }

//    /**
//     * @return TableET[] Returns an array of TableET objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TableET
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
