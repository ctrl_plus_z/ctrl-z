<?php

namespace App\Repository;

use App\Entity\Iot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Iot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Iot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Iot[]    findAll()
 * @method Iot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class IotRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Iot::class);
    }

//    /**
//     * @return Iot[] Returns an array of Iot objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Iot
    {
        return $this->createQueryBuilder('i')
            ->andWhere('i.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
