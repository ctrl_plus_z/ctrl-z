<?php

namespace App\Repository;

use App\Entity\TableW;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TableW|null find($id, $lockMode = null, $lockVersion = null)
 * @method TableW|null findOneBy(array $criteria, array $orderBy = null)
 * @method TableW[]    findAll()
 * @method TableW[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TableWRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TableW::class);
    }

//    /**
//     * @return TableW[] Returns an array of TableW objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TableW
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
