<?php


// src/Form/DeviceType.php
namespace App\Form;

use App\Entity\Device;
use App\Form\IotType;
use App\Entity\Iot;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

class DeviceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class)
            ->add('manufacturer', TextType::class)
            ->add('model', TextType::class)
            ->add('type', ChoiceType::class, array(
                'choices'=> array(
                    'Washmachine' => 'Washmachine',
                    'Dryer' => 'Dryer',
                    'Wash + Dryer' => 'WashAndDryer',
                    'Refrigerator' => 'Refrigerator',
                    'Television' => 'Television',
                    'Dish Washer' => 'DishWasher',
                    'Baking Oven' => 'BakingOven',
                    'Microwave Oven' => 'MicrowaveOven',
                    'Cooking stove' => 'CookingStove',
                    'Hot Plate' => 'HotPlate',
                    'Others' => 'Others'
                ),
            ))
            ->add('initialPrice', MoneyType::class)
            ->add('purchaseDate', DateTimeType::class)
            ->add('iot', CollectionType::class, array(
                'entry_type' => IotType::class,
                'allow_add' => true,
                'prototype' => true,
                'by_reference' => false,
            ))
            ->add('color', TextType::class)
            ->add('weight', NumberType::class)
            ->add('height', NumberType::class)
            ->add('width', NumberType::class)
            ->add('thickness', NumberType::class)
            ->add('productRemarks', TextType::class, array(
                'required' => false))
            ->add('serialNumber', TextType::class)
            ->add('lifeTime', NumberType::class)
            ->add('metrics', CollectionType::class, array(
                'entry_type' => TextType::class,
                'allow_add' => true,
                'prototype' => true,
                'required' => false,
                'empty_data' => [],
            ))
            ->add('metricDefinition', CollectionType::class, array(
                'entry_type' => TextType::class,
                'allow_add' => true,
                'prototype' => true,
                'required' => false,
                'empty_data' => [],
            ))
            ->add('metricMin', CollectionType::class, array(
                'entry_type' => NumberType::class,
                'allow_add' => true,
                'prototype' => true,
                'required' => false,
                'empty_data' => [],
            ))
            ->add('metricMax', CollectionType::class, array(
                'entry_type' => NumberType::class,
                'allow_add' => true,
                'prototype' => true,
                'required' => false,
                'empty_data' => [],
            ))
            ->add('metricSeverity', CollectionType::class, array(
                'entry_type' => ChoiceType::class,
                'entry_options' => array(
                    'choices' => array(
                        'Critical' => 0,
                        'Alert' => 1,
                        'Moderate' => 2,
                        'Mild' => 3,
                        'User Hint' => 4,
                    ),
                ),
                'allow_add' => true,
                'prototype' => true,
                'required' => false,
                'empty_data' => [],
            ))
            ->add('metricMessage', CollectionType::class, array(
                'entry_type' => TextType::class,
                'allow_add' => true,
                'prototype' => true,
                'required' => false,
                'empty_data' => [],
            ))
            ->add('imageFile', VichImageType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Device::class,
        ));
    }
}
