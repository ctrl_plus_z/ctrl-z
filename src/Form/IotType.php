<?php


// src/Form/IotType.php
namespace App\Form;

use App\Entity\Iot;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;




class IotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array('label' => 'Component', 'attr' => array('style' => 'width: 200px; margin-left: 2px')))
            ->add('sensorType', ChoiceType::class, array('label' => 'IoT Class', 'attr' => array('style' => 'width: 200px; margin-left: 10px'),
            'choices' => array(
                'Energy' => 'E',
                'Temperature' => 'T',
                'Energy, Temperature' => 'ET',
                'Weight' => 'W')));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Iot::class,
        ));
    }
}
