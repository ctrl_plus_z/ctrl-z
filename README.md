# Project: Ctrl+Z #
We would like to propose a business model which is aimed at bringing 0.5 billion private households in EU-28 (according to the 2011 census study) to the idea of a circular economy. This business idea mainly targets the home appliances sector.

## Why People in EU-28 prefer to buy new products? ##
Today in EU, we are witnessing a consumer attitude that it is economically a better choice to buy a new product rather than repairing the used malfunctioned product. This is mainly due to the reason that the significant labor cost plus the spare part cost compel the consumer to buy a new product. 

Another important factor to consider is migration. In this new world of globalization, migrating from one city to another for professional reasons is inevitable. Due to the significant cost involved due to the de-installation, transportation, and installation, consumers are limited to take only their precious possessions. Especially, the used home appliances are either sold through online platforms or simply dumped. 

The product manufacturers are also compelled to maintain an attractive price to their product in order to stay competitive in the market. Also, in some cases, the product dealers offer additional service such as offering free installation in the event of buying new products. This further favors the consumer to go for new products.

## How to change this trend? ##
From the above arguments, it is clear that monetary considerations and the comfort of consumers are the two important factors which are driving the above chain. The business idea which we offer can overcome these issues. By integrating the principles of functional economy and reverse logistics in the circular economy with technologies such as Internet of Things (IoT) and Blockchain concept, we have developed a business idea which has the potential to bring the whole home appliances industry into the circular economy successfully.

The home appliances which we have considered in this idea are Television set, cooking plate, refrigerators, oven, washing machine, microwave oven, and utensil washer. By adopting this business idea, a method can be created which ensures the active usage of 100% lifetime of a product in an effective way. After the effective product lifetime, this idea helps to track the product during its repair, re-use and regeneration phase.


## Functional Economy and Reverse Logistics ##
By adopting the functional economy principle, this business model rents the home appliances to the customer for a fixed contractual term agreed between the customer and the firm. During the contract period, the firm charges a monthly rental from the customer. The firm has been designed to offer a huge variety of options such as different brands, models, used/ new products through which the customer can pick and choose appliance based on his/ her need.

At the end of the contractual period, the customer can either renew or quit the contract. In the event of quitting the contract, the customer has to return the product. In order to encourage customers to the reverse logistics principle, plans have been designed to offer a monetary reward to the customer. This action will effectively bring back the product into the cycle again.

## Role of Internet of Things (IoT) ##
By bringing IoT into this business model, a control mechanism can be created between the customer and firm. Smart attachments are connected to all appliances, which can communicate data to both the parties here. For example, an appliance which has been promised to the customer to provide certain process/ energy efficiency at the beginning of the contract. Such parameters can be claimed during its operation. IoT also helps the customer to monitor the electric consumption and costs associated with it.

When you see the advantage of IoT from the firm's point of view, smart data help the firm to assess the degree of usage of a product. In the contractual conditions, the firm can say that a product can be used to a maximum of 'T' hours during its contractual period. The customer will be charged extra in case of an overutilization. So at the end of the contractual period, this data will help the firm in re-defining its lifetime, performance/ energy parameters and its rental value in the upcoming contracts.

## Role of Blockchain ##
Integrating the Blockchain concept into this idea will help to create a transparent ledger of events. This ledger will record the history of product usage. Information such as product performance parameters, component repair data, location, active usage time can be recorded at every stage. Such data will help the customer to choose the product to his/ her requirements and also brings faith in this system.

From the firm's point of view, this ledger will help to track a product from its entry till exit into the cycle, its performance parameters at the beginning/ end of a contractual period. This ledger can be also useful to record the set of events after the product's lifetime. Information such as its recycling data, usage of active components in some other products, raw materials reprocessing data etc will help the firm to evaluate the cycle efficiency, raw material flow etc even after the effective lifetime of the product.

## Important Functions of this Business Model ##

* Buying used home appliances from the market or directly from the consumer. By doing so, this action will bring products into the cycle.
* Storing and repairing the used products if needed in order to bring the product to its best possible working condition.
* Creating contracts with the customer.
* Installation, de-installation and monitoring the product performance if required at the customer's place.
* Setting up business understandings with original product manufactures in order to supply new products and spare parts of the products available in the market.


### Regards ###
### Team: Ctrl + Z ###
Team members:  
Christie, Louis Alappat  
Muralimohan, Juttu Vidyasagar  
Vishnu, Kanakakumar  
Vivian, Silas Jeyachander Manohar 
